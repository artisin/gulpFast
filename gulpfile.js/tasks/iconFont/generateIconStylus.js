var gulp         = require('gulp'),
    config       = require('../../config/iconFont'),
    swig         = require('gulp-swig'),
    rename       = require('gulp-rename'),
    handleErrors = require('../../lib/handleErrors');

module.exports = function(glyphs, options) {
  gulp.src(config.template)
    .pipe(swig({
      data: {
        icons: glyphs.map(function(glyph) {
          return {
          name: glyph.name,
          code: glyph.unicode[0].charCodeAt(0).toString(16).toUpperCase()
          };
        }),
        fontName: config.options.fontName,
        fontPath: config.fontPath,
        className: config.className,
        comment: 'Ey, This Was Generated by gulp/tasks/iconFont.js. I do not recommend you edit this directly but thats up to you big dog.'
      }
    }))
    .on('error', handleErrors)
    .pipe(rename(config.stylusOutputName))
    .pipe(gulp.dest(config.stylusDest));
};
